(ns redis-clj.pool-test
  (:require  [clojure.test :refer :all]
             [redis-clj.pool :as pool]))

(deftest initialize-test
  (testing "Can initialize the pool if the configuration is correct"
    (let [p (pool/init! {})]
      (is (= 0
             (.getNumActive p))
          (= 1
             (.getNumIdle p))))))

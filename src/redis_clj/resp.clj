(ns redis-clj.resp
  (:require [clojure.string :as string]
            [byte-streams :as bs]))

(defn- simple-string
  "Simple string encoder
   '+Okay\r\n' -> 'Okay'"
  ([resp]
   (simple-string resp 1))
  ([resp start-index]
   (let [end-index (string/last-index-of resp "\r\n")]
     (simple-string resp start-index end-index)))
  ([resp start-index end-index]
   (subs resp start-index end-index)))

(defmulti resp->str
  (fn [resp]
    (condp = (first resp)
      \+ :str
      \- :error
      \: :num
      \$ :bulk-str
      \* :array)))

(defmethod resp->str :str
  [resp]
  (simple-string resp))

(defmethod resp->str :error
  [resp]
  (simple-string resp))

(defmethod resp->str :num
  [resp]
  (Integer/parseInt (simple-string resp)))

(defmethod resp->str :bulk-str
  [resp]
  (let [length       (->> resp
                          (re-find #"\d+")
                          Integer/parseInt)
        start-index  (+ 2 (string/index-of resp "\r\n"))
        end-index    (string/last-index-of resp "\r\n")
        decoded-resp (simple-string resp start-index end-index)]
    decoded-resp))

(defmethod resp->str :array
  [resp]
  resp)

(defn parse
  "Convert RESP response into string"
  [resp]
  (resp->str (bs/convert resp String)))

(ns redis-clj.resp-test
  (:require [clojure.test :refer :all]
            [redis-clj.resp :as sut]))

(deftest simple-string
  (testing "Can convert from RESP simple strings"
    (let [test-str "+Okay\r\n"]
      (is (= "Okay"
             (sut/resp->str test-str))))))

(deftest error-string
  (testing "Can convert from RESP error strings"
    (let [err-str "-ERR Wrong operation\r\n"]
      (is (= "ERR Wrong operation"
             (sut/resp->str err-str))))))

(deftest number-string
  (testing "Can convert from RESP number strings"
    (let [num-str ":123\r\n"]
      (is (= 123
             (sut/resp->str num-str))))))

(deftest bulk-string
  (testing "Can convert from RESP bulk strings"
    (let [bulk-str "$5\r\nWowza\r\n"]
      (is (= "Wowza"
             (sut/resp->str bulk-str))))))

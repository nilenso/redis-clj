(ns redis-clj.operations
  (:refer-clojure :exclude [set])
  (:require [clojure.string :as string]
            [manifold.stream :as s]
            [manifold.deferred :as d]
            [redis-clj.pool :as pool]
            [redis-clj.resp :as resp]))

(defn op
  "Execute the redis operation asynchronously. Uses manifold deferred for execution as well as the result.

   Usage:
   deferred => (op \"set\" \"x\" 123)
   value => @(op \"set\" \"x\" 123)"
  [conn op & args]
  (d/let-flow [send-op (->> args
                            (cons op)
                            (string/join " ")
                            (#(str % "\r\n"))
                            (s/put! conn))
               recv-op (s/take! conn)]
    (resp/parse recv-op)))

(defmacro with-connection
  "Performs redis operations on a pooled connection.
   Returns an array of responses from Redis server. The responses are ordered the same as input

   Usage:
   (with-connection [conn connection-pool]
     (op conn \"set\" \"x\" \"value\")
     (op conn \"get\" \"x\"))"
  [[conn pool] & body]
  `(let [~conn (pool/get-resource ~pool)
         resp# (conj [] ~@body)]
     (pool/release-resource ~pool ~conn)
     resp#))

(defmacro with-transaction
  "Performs a redis transaction on a pooled connection.

   Usage:
   (with-transaction [conn connection-pool]
     (op conn \"set\" \"x\" \"value\"))"
  [[conn pool] & body]
  `(let [~conn  (pool/get-resource ~pool)
         multi# (op ~conn "multi")
         queue# (conj [] ~@body)
         resp#  (conj queue# (op ~conn "exec"))]
     (pool/release-resource ~pool ~conn)
     resp#))

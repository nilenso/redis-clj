(defproject redis-clj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :local-repo ".m2"
  :dependencies [[aleph "0.4.4"]
                 [byte-streams "0.2.3"]
                 [org.apache.commons/commons-pool2 "2.5.0"]
                 [org.clojure/clojure "1.8.0"]]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})

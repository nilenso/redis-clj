(ns redis-clj.pool
  (:require [aleph.tcp :as tcp]
            [manifold.stream :as s])
  (:import (org.apache.commons.pool2 PooledObjectFactory
                                     PooledObject)
           (org.apache.commons.pool2.impl GenericObjectPool
                                          GenericObjectPoolConfig
                                          AbandonedConfig)
           (org.apache.commons.pool2.impl DefaultPooledObject)))

(def default-pool-config
  {:max-idle  1
   :max-total 5})

(def default-connection-config
  {:host "localhost"
   :port 6379})

(def default-abandoned-config
  {})

(defn init!
  "Creates a pool of Redis clients backed by the apache.commons.pool"
  [{:keys [pool-config connection-config abandoned-config]
    :or   {pool-config       default-pool-config
           connection-config default-connection-config
           abandoned-config  default-abandoned-config}}]
  (let [pool-factory          (proxy [PooledObjectFactory] []
                                (makeObject []
                                  (DefaultPooledObject.
                                   @(tcp/client connection-config)))
                                (destroyObject
                                  [^PooledObject pooled-client]
                                  (s/close! (.getObject pooled-client)))
                                (validateObject
                                  [^PooledObject pooled-client]
                                  (or (not (s/closed? (.getObject pooled-client))))) ;; Add a ping check
                                (activateObject
                                  [^PooledObject pooled-client]
                                  ;; client socket is already activated
                                  )
                                (passivateObject
                                  [^PooledObject pooled-client]
                                  ;; Called when using addObject method to add pooled objects to the pool
                                  ;; I am not sure how to make a socket as passive
                                  ;; Maybe not start it at all, but in that case activateObject would need
                                  ;; to do something. So leaving both activate and passivate as no-op for now
                                  ))
        pool-configuration    (doto (GenericObjectPoolConfig.)
                                (.setMaxIdle (:max-idle pool-config))
                                (.setMaxTotal (:max-total pool-config)))
        abandon-configuration (doto (AbandonedConfig.))
        pool                  (doto (GenericObjectPool. pool-factory
                                                        pool-configuration
                                                        abandon-configuration)
                                (.preparePool))]
    pool))

(defn destroy!
  [^GenericObjectPool pool]
  (.close pool))

(defn get-resource
  [^GenericObjectPool pool]
  (.borrowObject pool))

(defn release-resource
  [^GenericObjectPool pool pooled-object]
  (.returnObject pool pooled-object))
